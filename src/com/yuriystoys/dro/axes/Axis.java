/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.axes;

public final class Axis {
	public static final int NONE = -1;
	// primary axes
	public static final int X = 0;
	public static final int Y = 1;
	public static final int Z = 2;

	// secondary linear axes
	public static final int U = 3;
	public static final int V = 4;
	public static final int W = 5;

	// secondary rotary axes
	public static final int R = 6;
	public static final int S = 7;
	public static final int T = 8;

	// secondary angular axes
	public static final int A = 9;
	public static final int B = 10;
	public static final int C = 11;

	public static AxisType getAxisType(int axis) {
		switch (axis) {
		case Axis.A:
		case Axis.B:
		case Axis.C: {
			return AxisType.Angular;
		}
		case Axis.R:
		case Axis.S:
		case Axis.T: {
			return AxisType.Rotary;
		}
		default: {
			return AxisType.Linear;
		}
		}
	}

	public enum AxisType {
		Linear, Rotary, Angular
	}
}
