package com.yuriystoys.dro.axes;

public class SummingAxisReadoutProcessor extends AxisReadoutProcessor {

	private final AxisSettings targetAxis;
	private final double axisScale;
	
	public SummingAxisReadoutProcessor(AxisSettings originAxis, AxisSettings targetAxis) {
		super(originAxis);
		
		this.targetAxis = targetAxis;
		
		axisScale =  targetAxis.getCountsPerInch() / axis.getCountsPerInch() * (axis.isInverted() ? -1 : 1);
	}

	@Override
	public void accept(int position) {
		
		//convert the position's CPI to target axes CPI
		targetAxis.setSecondaryPosition((int) (position * axisScale));
	}
}
