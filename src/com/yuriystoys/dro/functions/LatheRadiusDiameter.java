package com.yuriystoys.dro.functions;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.axes.AxisSettings;

public class LatheRadiusDiameter extends IDroFunction {

	private final ImageButton button;
	private boolean diameter;
	private AxisSettings axis;

	public LatheRadiusDiameter(final Activity activity, ViewGroup target) {
		super(activity, target, MachineTypes.LATHE);

		final View view = activity.getLayoutInflater().inflate(R.layout._button_radius_diameter, target);
		
		axis = DroApplication.getCurrentInstance().getDro().getAxis(Axis.X);

		button = (ImageButton) view.findViewById(R.id.radiusDiaButton);

		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				diameter = !diameter;
				
				if(diameter)
				{
					button.setImageDrawable(activity.getResources().getDrawable(R.drawable.diameter));
					axis.setScale(2);
				}
				else
				{
					button.setImageDrawable(activity.getResources().getDrawable(R.drawable.radius));
					axis.clearScale();
				}
			}
		});
	}

	@Override
	public void showForMachine(MachineTypes type) {
		button.setVisibility(type == this.type ? View.VISIBLE : View.GONE);
	}
}
