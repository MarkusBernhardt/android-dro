package com.yuriystoys.dro.functions;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.ui.HoleCircleDialog;

public class MillHoleCircle extends IDroFunction {

	private final View button;
	
	public MillHoleCircle(Activity activity, ViewGroup target) {
		super(activity, target, MachineTypes.VERTICAL_MILL);

		View view =activity.getLayoutInflater().inflate(R.layout._button_hole_circle, target);
		
		button = view.findViewById(R.id.holeCircleButton);
		
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				HoleCircleDialog.Show(MillHoleCircle.this.activity);
			}
		});
	}

	@Override
	public void showForMachine(MachineTypes type) {
		button.setVisibility(type == this.type ? View.VISIBLE : View.GONE);
	}
}
