package com.yuriystoys.dro.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.HexDump;

public class UsbDroDriver extends DroDriver {

	private static final String ACTION_USB_PERMISSION = "com.yuriystoys.dro.USB_PERMISSION";

	private final ReadoutHandler handler;
	private final int baudRate;
	private UsbService service;
	//private final String TAG = UsbDroDriver.class.getSimpleName();

	private final UsbManager manager;

	private final PendingIntent intent;
	// Find the first available driver.
	UsbSerialDriver driver;

	public UsbDroDriver(Context context, int baudRate) {
		super(context);

		this.baudRate = baudRate;
		manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);

		intent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
		IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
		context.registerReceiver(mUsbReceiver, filter);

		//
		handler = new ReadoutHandler();
		//
	}

	@Override
	public void connect(String deviceName) {
		
		timeoutHandler.removeMessages(WATCHDOG_REFRESH);
		timeoutHandler.removeMessages(WATCHDOG_DISCONNECT_INACTIVE);
		timeoutHandler.removeMessages(WATCHDOG_DISCONNECT);
		
		// need to request the permission ot talk to USB
		if (service == null) {
			HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
			UsbDevice d = null;
			for (String s : deviceList.keySet()) {
				d = deviceList.get(s);
			}

			if(d == null)
				throw new IllegalStateException("USB device not found");
			
			manager.requestPermission(d, intent);
		} else {
			service.connect();
		}
	}

	@Override
	public void disconnect() {

		timeoutHandler.removeMessages(WATCHDOG_REFRESH);
		timeoutHandler.removeMessages(WATCHDOG_DISCONNECT_INACTIVE);
		timeoutHandler.removeMessages(WATCHDOG_DISCONNECT);

		if (service != null)
		{
			service.stop();
			service = null;
		}
	}

	@Override
	public void reconnect() {
		// TODO Auto-generated method stub
		if (service != null && getState() == ConnectionState.DISCONNECTED)
			service.connect();

		// start watching the connection
		timeoutHandler.removeMessages(WATCHDOG_REFRESH);
	}

	@Override
	public int getState() {
		// If the adapter is null, then BlueTooth is not supported
		if (service == null)
			return ConnectionState.NO_SERVICE;
		else {
			return connectionState;
		}
	}

	public String[] getPairedDeviceNames() {

		List<String> names = new ArrayList<String>();

		for (final UsbDevice device : manager.getDeviceList().values()) {
			final UsbSerialDriver driver = UsbSerialProber.acquire(manager, device);
			//Log.d(TAG, "Found usb device: " + device);
			if (driver != null) {
				names.add(HexDump.toHexString((short) device.getDeviceId()));

			}
		}
		return names.toArray(new String[0]);
	}

	private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (ACTION_USB_PERMISSION.equals(action)) {
				synchronized (this) {
					UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

					if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
						if (device != null) {
							// call method to set up device communication
							driver = UsbSerialProber.acquire(manager, device);
							service = new UsbService(manager, handler, baudRate);
							UsbDroDriver.this.connect(device.getDeviceName());
						}
					} else {
						//Log.d(TAG, "permission denied for device " + device);
					}
				}
			}
		}
	};
}
