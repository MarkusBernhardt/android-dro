package com.yuriystoys.dro.ui;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.tools.Tool;

public class SetDimensionDialog extends DialogFragment {

	int toolId = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_tool_offset, null);

		setUpUi(view);
		fillSpinner();

		if (toolId > 0) {

			for (int i = 0; i < _toolSpinner.getCount(); i++) {
				Tool tool = ((Tool) _toolSpinner.getItemAtPosition(i));
				if (tool.getId() != null && toolId == tool.getId()) {
					_toolSpinner.setSelection(i);
					break;
				}
			}
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.tool_ofset_title);
		builder.setCancelable(false);
		builder.setView(view);

		// set dialog message
		builder.setCancelable(false).setPositiveButton(R.string.tool_ofset_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// do nothing. The real handler is below
			}
		}).setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		});

		// create alert dialog
		Dialog dialog = builder.create();

		dialog.setCanceledOnTouchOutside(false);

		dialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				Window window = ((AlertDialog) dialog).getWindow();

				if (getResources().getConfiguration().screenHeightDp < 400)
					window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

				((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (setOffsets())
									dismiss();
							}
						});

				EditText.OnKeyListener listener = new EditText.OnKeyListener() {

					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						if (keyCode == KeyEvent.KEYCODE_ENTER) {
							if (setOffsets())
								dismiss();
							return true;
						}
						return false;
					}
				};

				_toolDiameterEdit.setOnKeyListener(listener);
			}
		});

		return dialog;
	}

	private void fillSpinner() {
		List<Tool> tools = Repository.open(getActivity()).getTools(MachineTypes.VERTICAL_MILL);

		Tool temp = new Tool();
		temp.setName("-- Select --");
		temp.setFluteDiameter(0D);
		temp.setOffsetZ(0D);

		tools.add(0, temp);

		ArrayAdapter<Tool> dataAdapter = new ArrayAdapter<Tool>(getActivity(), android.R.layout.simple_spinner_item,
				tools);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		_toolSpinner.setAdapter(dataAdapter);

		_toolSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

				Tool tool = (Tool) parent.getItemAtPosition(position);
				Dro dro = DroApplication.getCurrentInstance().getDro();

				double diameter = dro.isInMetricMode() ? tool.getFluteDiameter() * Dro.MM_PER_INCH : tool
						.getFluteDiameter();

				double zOffset = dro.isInMetricMode() ? tool.getOffsetZ() * Dro.MM_PER_INCH : tool.getOffsetZ();

				_toolDiameterEdit.setText(dro.getCurrentFormat().format(diameter));
				_zOffsetEdit.setText(dro.getCurrentFormat().format(zOffset));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		_toolSpinner.setEnabled(tools.size() > 1);
	}

	private boolean setOffsets() {
		double radius = 0;
		double zOffset = 0;
		int xMultiplier = 0;
		int yMultiplier = 0;

		Resources res = getActivity().getResources();

		try {
			radius = Double.parseDouble(_toolDiameterEdit.getEditableText().toString()) / 2;
		} catch (NumberFormatException ex) {
			Toast.makeText(getActivity(), R.string.tool_diameter_invalid, Toast.LENGTH_LONG).show();
			return false;
		}

		if (!isEmpty(_zOffsetEdit)) {
			try {
				zOffset = Double.parseDouble(_zOffsetEdit.getEditableText().toString()) / 2;
			} catch (NumberFormatException ex) {
				Toast.makeText(getActivity(), R.string.tool_z_offset_invalid, Toast.LENGTH_LONG).show();
				return false;
			}
		}

		int xSelected = _xOffsetSpinner.getSelectedItemPosition();

		if (xSelected != Spinner.INVALID_POSITION) {
			xMultiplier = res.getIntArray(R.array.ToolOffsetMultipliers)[xSelected];
		}

		int ySelected = _yOffsetSpinner.getSelectedItemPosition();

		if (ySelected != Spinner.INVALID_POSITION) {
			yMultiplier = res.getIntArray(R.array.ToolOffsetMultipliers)[ySelected];
		}

		if (xMultiplier == 0 && yMultiplier == 0 && zOffset == 0) {
			Toast.makeText(getActivity(), R.string.tool_offset_invalid, Toast.LENGTH_LONG).show();
			return false;
		}

		Dro dro = ((DroApplication) getActivity().getApplication()).getDro();

		dro.getAxis(Axis.X).setToolOffset(radius * xMultiplier);
		dro.getAxis(Axis.Y).setToolOffset(radius * yMultiplier);
		dro.getAxis(Axis.Z).setToolOffset(zOffset);

		return true;
	}

	private Spinner _toolSpinner;
	private Spinner _xOffsetSpinner;
	private Spinner _yOffsetSpinner;

	private EditText _toolDiameterEdit;
	private EditText _zOffsetEdit;

	private void setUpUi(View view) {

		_toolSpinner = (Spinner) view.findViewById(R.id.toolSpinner);

		_toolDiameterEdit = (EditText) view.findViewById(R.id.toolDiameterEdit);
		_toolDiameterEdit.setText(R.string.zero_thousandths);

		_toolSpinner = (Spinner) view.findViewById(R.id.toolSpinner);

		_toolSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				_toolDiameterEdit.setText(R.string.zero_thousandths);
			}
		});

		_xOffsetSpinner = (Spinner) view.findViewById(R.id.xOffsetSpinner);
		_yOffsetSpinner = (Spinner) view.findViewById(R.id.yOffsetSpinner);
		_zOffsetEdit = (EditText) view.findViewById(R.id.zOffsetEdit);

		Dro dro = DroApplication.getCurrentInstance().getDro();
		String units = view.getContext().getResources().getString(dro.isInMetricMode() ? R.string.mm : R.string.inch);

		((TextView) view.findViewById(R.id.diameterUnits)).setText(units);
		((TextView) view.findViewById(R.id.zOffsetUnits)).setText(units);
	}

	private boolean isEmpty(EditText etText) {
		return etText.getText().toString().trim().length() == 0;
	}

	public static void Show(Activity activity) {

		Show(activity, -1);
	}

	public static void Show(Activity activity, int toolId) {

		FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
		Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		DialogFragment newFragment = new SetDimensionDialog();
		((SetDimensionDialog) newFragment).toolId = toolId;

		newFragment.show(ft, "dialog");
	}

}
