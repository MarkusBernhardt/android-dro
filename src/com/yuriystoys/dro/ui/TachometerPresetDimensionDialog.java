package com.yuriystoys.dro.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.AxisSettings;
import com.yuriystoys.dro.core.Dro;

public class TachometerPresetDimensionDialog extends DialogFragment {

	AxisSettings axis = null;
	private static boolean isShowing = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		// TODO Auto-generated method stub
		super.onDismiss(dialog);
		isShowing = false;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_tachometer_set_dimension, null);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Set RPM");
		builder.setCancelable(false);
		builder.setView(view);

		// set dialog message
		builder.setCancelable(false)
				.setPositiveButton(R.string.linear_axis_preset_ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do nothing. The real handler is below
					}
				}).setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		Dialog dialog = builder.create();

		dialog.setCanceledOnTouchOutside(false);

		dialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				Window window = ((AlertDialog) dialog).getWindow();

				if (getResources().getConfiguration().screenHeightDp < 400)
					window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

				((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (setDimension())
									dismiss();
							}
						});

			}
		});

		setUi(view);

		return dialog;
	}

	EditText _dimension;

	private void setUi(View view) {
		Dro dro = DroApplication.getCurrentInstance().getDro();

		_dimension = ((EditText) view.findViewById(R.id.dimension));

		_dimension.setText(dro.getTachometerFormat().format(axis.getReadout()));
	}

	private boolean setDimension() {
		double dimension = 0D;
		Dro dro = DroApplication.getCurrentInstance().getDro();

		if (!isEmpty(_dimension)) {

			String[] parts = _dimension.getEditableText().toString().split("/");

			if (parts.length == 2) {

				double enumerator, denominator;

				try {
					enumerator = Double.parseDouble(parts[0]);
					denominator = Double.parseDouble(parts[1]);
				} catch (NumberFormatException ex) {
					Toast.makeText(getActivity(), R.string.linear_axis_dimension_invalid, Toast.LENGTH_LONG).show();
					return false;
				}

				dimension = enumerator / denominator;

				_dimension.setText(dro.getCurrentFormat().format(dimension));

			} else {
				try {
					dimension = Double.parseDouble(parts[0]);
				} catch (NumberFormatException ex) {
					Toast.makeText(getActivity(), R.string.linear_axis_dimension_invalid, Toast.LENGTH_LONG).show();
					return false;
				}
			}

			int position = axis.convertToCounts(dimension);

			axis.setAbsoluteOffset(position - axis.getRawPosition());
			dismiss();

		} else {
			Toast.makeText(getActivity(), R.string.linear_axis_dimension_invalid, Toast.LENGTH_LONG).show();
			return false;
		}

		return false;
	}

	private boolean isEmpty(EditText etText) {
		return etText.getText().toString().trim().length() == 0;
	}

	public static void Show(Activity activity, int targetAxis) {

		if (!isShowing) {
			isShowing = true;
			FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
			Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
			if (prev != null) {
				ft.remove(prev);
			}
			ft.addToBackStack(null);

			// Create and show the dialog.
			DialogFragment newFragment = new TachometerPresetDimensionDialog();
			((TachometerPresetDimensionDialog) newFragment).axis = DroApplication.getCurrentInstance().getDro()
					.getAxis(targetAxis);

			newFragment.show(ft, "dialog");
		}
	}

}
