/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

import com.yuriystoys.dro.R;

public class ListTabsFragment extends Fragment implements OnTabChangeListener {

	public static final String TAB_POINTS = "points";
	public static final String TAB_TOOLS = "tools";

	private TabHost tabHost;
	private int currentTab = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout._fragment_list_tabs, container, false);

		tabHost = (TabHost) root.findViewById(R.id.tabHost);

		setupTabs();

		return root;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//setRetainInstance(true);

		tabHost.setOnTabChangedListener(this);
		tabHost.setCurrentTab(currentTab);
		// manually start loading stuff in the first tab
		onTabChanged(TAB_POINTS);
	}

	@Override
	public void onResume() {

		super.onResume();
	}

	private void setupTabs() {

		tabHost.setup(); // you must call this before adding your tabs!
		tabHost.addTab(newTab(TAB_POINTS, "Points", R.id.tab1));
		tabHost.addTab(newTab(TAB_TOOLS, "Tools", R.id.tab2));
	}

	private TabSpec newTab(String tag, String label, int tabContentId) {
		TabSpec tabSpec = tabHost.newTabSpec(tag);
		tabSpec.setIndicator(label);
		tabSpec.setContent(tabContentId);
		return tabSpec;
	}

	@Override
	public void onTabChanged(String tabId) {
		FragmentManager fm = getActivity().getSupportFragmentManager();

		if (TAB_POINTS.equals(tabId)) {
			if (fm.findFragmentByTag(tabId) == null) {
				fm.beginTransaction().replace(R.id.tab1, (Fragment)new PointListFragment(), tabId).commit();
			}
			currentTab = 0;
			return;
		}
		if (TAB_TOOLS.equals(tabId)) {
			if (fm.findFragmentByTag(tabId) == null) {
				fm.beginTransaction().replace(R.id.tab2, (Fragment)new ToolListFragment(), tabId).commit();
			}
			currentTab = 1;
			return;
		}
	}

}
