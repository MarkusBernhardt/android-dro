package com.yuriystoys.dro.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.tools.Tool;
import com.yuriystoys.dro.ui.util.ToolsAdapter;

@SuppressLint("ValidFragment")
public class MillToolEditDialog extends DialogFragment {

	public static final String TOOL_ID_KEY = "tool_id";
	private int toolId = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		// int toolId = savedInstanceState != null ?
		// savedInstanceState.getInt(TOOL_ID_KEY, -1) : -1;

		View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_mill_tool_edit, null);

		setUpUi(view);
		if(toolId>=0)
			showTool(toolId);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.tool_ofset_title);
		builder.setCancelable(false);
		builder.setView(view);

		// set dialog message
		builder.setCancelable(false).setPositiveButton(R.string.mill_tool_edit_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// do nothing. The real handler is below
			}
		}).setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		});

		// create alert dialog
		Dialog dialog = builder.create();
		
		dialog.setCanceledOnTouchOutside(false);

		dialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				Window window = ((AlertDialog) dialog).getWindow();

				if (getResources().getConfiguration().screenHeightDp < 400)
					window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

				((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (saveTool())
									dismiss();
							}
						});
			}
		});

		return dialog;
	}

	private EditText _toolNameEdit;
	private EditText _toolDescriptionEdit;
	private EditText _toolDiameterEdit;
	private EditText _toolToothCountEdit;
	private EditText _toolZOffsetEdit;

	private void setUpUi(View view) {
		
		Dro dro = DroApplication.getCurrentInstance().getDro();
		String units = view.getContext().getResources().getString(dro.isInMetricMode() ? R.string.mm : R.string.inch);

		((TextView) view.findViewById(R.id.diameterUnits)).setText(units);
		((TextView) view.findViewById(R.id.zOffsetUnits)).setText(units);

		_toolNameEdit = (EditText) view.findViewById(R.id.toolNameEdit);
		_toolDescriptionEdit = (EditText) view.findViewById(R.id.toolDescriptionEdit);
		_toolDiameterEdit = (EditText) view.findViewById(R.id.toolDiameterEdit);
		_toolToothCountEdit = (EditText) view.findViewById(R.id.toolToothCount);
		_toolZOffsetEdit = (EditText) view.findViewById(R.id.toolZOffsetEdit);

		_toolZOffsetEdit.setText(dro.getCurrentFormat().format(0));



	}

	private boolean saveTool() {

		if (isEmpty(_toolNameEdit)) {
			Toast.makeText(getActivity(), "Tool name is required.", Toast.LENGTH_LONG).show();
			_toolNameEdit.requestFocus();
			return false;
		}

		if (isEmpty(_toolDiameterEdit)) {
			Toast.makeText(getActivity(), "Tool diameter is required.", Toast.LENGTH_LONG).show();
			_toolDiameterEdit.requestFocus();
			return false;
		}

		if (isEmpty(_toolToothCountEdit)) {
			Toast.makeText(getActivity(), "Tooth/flute count is required.", Toast.LENGTH_LONG).show();
			_toolDiameterEdit.requestFocus();
			return false;
		}

		String name = _toolNameEdit.getText().toString();
		String description = _toolDescriptionEdit.getText().toString();

		String tempStr = _toolDiameterEdit.getText().toString();

		double diameter = 0;
		double offset = 0;

		try {
			diameter = Double.parseDouble(tempStr);
		} catch (NumberFormatException ex) {
			Toast.makeText(getActivity(), "Please enter a valid tool diameter.", Toast.LENGTH_LONG).show();
			_toolDiameterEdit.requestFocus();
			return false;
		}

		if (diameter == 0D) {
			Toast.makeText(getActivity(), "Please enter a valid tool diameter.", Toast.LENGTH_LONG).show();
			_toolDiameterEdit.requestFocus();
			return false;
		}

		Dro dro = DroApplication.getCurrentInstance().getDro();

		if (dro.isInMetricMode()) {
			// convert to inches
			diameter = diameter / Dro.MM_PER_INCH;
		}

		// Process Z offset

		tempStr = _toolZOffsetEdit.getText().toString();

		try {
			offset = Double.parseDouble(tempStr);
		} catch (NumberFormatException ex) {
			Toast.makeText(getActivity(), "Please enter a valid tool Z offset.", Toast.LENGTH_LONG).show();
			_toolZOffsetEdit.requestFocus();
			return false;
		}

		if (dro.isInMetricMode()) {
			// convert to inches
			offset = offset / Dro.MM_PER_INCH;
		}

		int toothCount = 0;

		tempStr = _toolToothCountEdit.getText().toString();

		try {
			toothCount = Integer.parseInt(tempStr);
		} catch (NumberFormatException ex) {
			Toast.makeText(getActivity(), "Please enter a valid tooth count.", Toast.LENGTH_LONG).show();
			_toolToothCountEdit.requestFocus();
			return false;
		}
		
		if (toothCount == 0) {
			Toast.makeText(getActivity(), "Please enter a valid tooth/flute count.", Toast.LENGTH_LONG).show();
			return false;
		}

		// save the tool

		Tool tool = new Tool();

		tool.setId(toolId);
		tool.setName(name);
		tool.setDescription(description);
		tool.setFluteDiameter(diameter);
		tool.setToothCount(toothCount);
		tool.setOffsetZ(offset);
		tool.setType(MachineTypes.VERTICAL_MILL);

		Repository repo = Repository.open(getActivity());

		if (toolId <= 0 && repo.insert(tool) == 0) {
			Toast.makeText(getActivity(), "Failed to create a new tool record.", Toast.LENGTH_LONG).show();
			return false;
		} else if (toolId > 0 && !repo.update(tool)) {
			Toast.makeText(getActivity(), "Failed to update the tool record.", Toast.LENGTH_LONG).show();
			return false;
		}

		ToolsAdapter.raiseOnToolListChanged();
		return true;
	}
	
	private boolean showTool(int toolId)
	{
		Dro dro = DroApplication.getCurrentInstance().getDro();
		Repository repo = Repository.open(getActivity());
		
		Tool tool = repo.getTool(toolId);
		if(tool==null)
			return false;
		
		_toolNameEdit.setText(tool.getName());
		_toolDescriptionEdit.setText(tool.getDescription());
		
		
		_toolToothCountEdit.setText(Integer.toString(tool.getToothCount()));
		
		double diameter = dro.isInMetricMode() ? tool.getFluteDiameter() * Dro.MM_PER_INCH : tool.getFluteDiameter();
		
		_toolDiameterEdit.setText(dro.getCurrentFormat().format(diameter));
		
		double offset = dro.isInMetricMode() ? tool.getOffsetZ() * Dro.MM_PER_INCH : tool.getOffsetZ();
		
		_toolZOffsetEdit.setText(dro.getCurrentFormat().format(offset));
		
		return true;
	}

	protected MillToolEditDialog(int toolId) {
		this.toolId = toolId;
	}

	public static void Show(Activity activity, int toolId) {

		FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
		Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		DialogFragment newFragment = new MillToolEditDialog(toolId);
		newFragment.show(ft, "dialog");
	}

	public static void Show(Activity activity) {

		FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
		Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		DialogFragment newFragment = new MillToolEditDialog(-1);
		newFragment.show(ft, "dialog");
	}

	private boolean isEmpty(EditText etText) {
		return etText.getText().toString().trim().length() == 0;
	}
}
