/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.ui;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.callbacks.IGlobalModeChangedCallback;
import com.yuriystoys.dro.callbacks.IGlobalUnitsChangedCallback;
import com.yuriystoys.dro.callbacks.IPointListChangedCallback;
import com.yuriystoys.dro.callbacks.IWorkspaceChanged;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.core.Point;
import com.yuriystoys.dro.core.Workspace;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.settings.BundleKeys;
import com.yuriystoys.dro.settings.PreferenceKeys;
import com.yuriystoys.dro.ui.util.PointsAdapter;
import com.yuriystoys.dro.ui.util.PointsAdapter.PointViewModel;
import com.yuriystoys.dro.ui.util.WorskpaceAdapter;
import com.yuriystoys.dro.ui.util.WorskpaceAdapter.WorkspaceViewModel;

public class PointListFragment extends Fragment implements IWorkspaceChanged, IPointListChangedCallback,
		IGlobalUnitsChangedCallback, IGlobalModeChangedCallback {
	// Workspace stuff
	private PointsAdapter pointsAdapter;
	private WorskpaceAdapter worskpaceAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout._fragment_points_list, container, false);
	}

	@Override
	public void onPause() {
		pointsAdapter = null;

		DroApplication app = (DroApplication) getActivity().getApplication();

		app.removeCallback((IWorkspaceChanged) this);
		app.getCurrentWorkspace().removeCallback((IPointListChangedCallback) this);
		app.getDro().removeCallback((IGlobalModeChangedCallback) this);
		app.getDro().removeCallback((IGlobalUnitsChangedCallback) this);

		super.onPause();
	}

	@Override
	public void onResume() {

		super.onResume();

		DroApplication app = ((DroApplication) getActivity().getApplication());

		app.registerCallback((IWorkspaceChanged) this);

		app.getDro().registerCallback((IGlobalModeChangedCallback) this);
		app.getDro().registerCallback((IGlobalUnitsChangedCallback) this);

		initWorkspaceList();
		initPointsList();

		Workspace workspace = ((DroApplication) getActivity().getApplication()).getCurrentWorkspace();

		if (workspace != null && workspace.getId() != null) {
			Spinner workspaceSpinner = ((Spinner) getView().findViewById(R.id.workspace_spinner));

			WorskpaceAdapter adapter = (WorskpaceAdapter) workspaceSpinner.getAdapter();

			for (int i = 0; i < adapter.getCount(); i++) {
				if (((WorkspaceViewModel) adapter.getItem(i)).id == workspace.getId()) {
					workspaceSpinner.setSelection(i);
					break;
				}
			}
		} else {
			throw new IllegalStateException("Workspace not set");
		}

		workspace.registerCallback((IPointListChangedCallback) this);

		pointsAdapter.refresh(workspace);
	}

	public void initWorkspaceList() {
		final Spinner workspaceSpinner = (Spinner) getView().findViewById(R.id.workspace_spinner);

		worskpaceAdapter = new WorskpaceAdapter(getActivity(), R.layout.list_item_workspace,
				new ArrayList<WorkspaceViewModel>());

		workspaceSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

				Workspace workspace = ((DroApplication) getActivity().getApplication()).getCurrentWorkspace();

				WorkspaceViewModel item = (WorkspaceViewModel) parent.getItemAtPosition(position);

				if (item.id == workspace.getId())
					return; // no need to mess with it...

				Repository repo = Repository.open(getActivity());

				Workspace ws = repo.getWorkspace(item.id);

				if (ws != null) {
					((DroApplication) getActivity().getApplication()).setCurrentWorkspace(ws);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		workspaceSpinner.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {

				if (workspaceSpinner.getSelectedItem() != null) {

					// outer dialog

					View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_workspace_commands, null);

					// create tools dialog

					Builder toolDialogBuilder = new Builder(getActivity()).setTitle(R.string.workspace_commands_title)
							.setView(view)
							.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {

								}
							});

					final AlertDialog toolDialog = toolDialogBuilder.create();

					View temp;

					// set event listeners

					temp = view.findViewById(R.id.viewWorskpaceButton);
					if (temp != null && temp instanceof Button) {

						((Button) temp).setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								toolDialog.dismiss();

								int workspaceId = ((WorkspaceViewModel) workspaceSpinner.getSelectedItem()).id;

								Intent intent = new Intent(getActivity(), WorkspacePreviewActivity.class);
								intent.putExtra(BundleKeys.WORKSPACE_ID, workspaceId);
								getActivity().startActivity(intent);
							}
						});
					}

					temp = view.findViewById(R.id.addWorskpaceButton);
					if (temp != null && temp instanceof Button) {

						((Button) temp).setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								toolDialog.dismiss();

								final EditText input = new EditText(PointListFragment.this.getActivity());

								Builder dialog = new AlertDialog.Builder(PointListFragment.this.getActivity())
										.setTitle(R.string.add_workspace_title)
										.setMessage(R.string.add_workspace_name_label)
										.setView(input)
										.setNegativeButton(R.string.cancel_button,
												new DialogInterface.OnClickListener() {
													public void onClick(DialogInterface dialog, int whichButton) {
													}
												});

								dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int whichButton) {
										Editable value = input.getText();

										SharedPreferences prefs = PreferenceManager
												.getDefaultSharedPreferences(getActivity().getApplicationContext());
										int bankId = Integer.parseInt(prefs.getString(PreferenceKeys.PREFERENCE_BANK,
												"0"));

										Repository repo = Repository.open(getActivity());

										if (repo.getWorkspace(bankId, value.toString()) != null) {
											Toast.makeText(getActivity(), R.string.add_workspace_error,
													Toast.LENGTH_LONG).show();
											return;
										}

										Workspace newWorksapce = Workspace.create(getActivity());
										newWorksapce.setName(value.toString());

										repo.insertWorkspace(newWorksapce);

										Spinner workspaceSpinner = (Spinner) PointListFragment.this.getView()
												.findViewById(R.id.workspace_spinner);

										WorskpaceAdapter adapter = (WorskpaceAdapter) workspaceSpinner.getAdapter();

										adapter.refresh();

										for (int i = 0; i < adapter.getCount(); i++) {
											if (((WorkspaceViewModel) adapter.getItem(i)).id == newWorksapce.getId()) {
												workspaceSpinner.setSelection(i);
												return;
											}
										}

										// ((DroApplication)getActivity().getApplication()).setCurrentWorkspace(newWorksapce);

									}
								});

								dialog.show();
							}
						});
					}

					temp = view.findViewById(R.id.deleteWorskpaceButton);
					if (temp != null && temp instanceof Button) {

						((Button) temp).setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {

								toolDialog.dismiss();

								Builder dialog = new AlertDialog.Builder(PointListFragment.this.getActivity())
										.setTitle(R.string.delete_workspace_title)
										.setMessage(R.string.delete_workspace_message)
										.setNegativeButton(R.string.cancel_button,
												new DialogInterface.OnClickListener() {
													public void onClick(DialogInterface dialog, int whichButton) {
													}
												});

								dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int whichButton) {

										Repository repo = Repository.open(getActivity());

										try {
											repo.delete(repo.getWorkspace(((WorkspaceViewModel) workspaceSpinner
													.getSelectedItem()).id));
										} catch (IllegalStateException ex) {
											Toast.makeText(PointListFragment.this.getActivity(), ex.getMessage(),
													Toast.LENGTH_LONG).show();
											return;
										}

										((WorskpaceAdapter) workspaceSpinner.getAdapter()).refresh();
										workspaceSpinner.setSelection(0);
									}
								});

								dialog.show();
							}
						});
					}

					temp = view.findViewById(R.id.clearWorskpaceButton);
					if (temp != null && temp instanceof Button) {

						((Button) temp).setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {

								toolDialog.dismiss();

								Builder dialog = new AlertDialog.Builder(PointListFragment.this.getActivity())
										.setTitle(R.string.clear_workspace_title)
										.setMessage(R.string.clear_workspace_message)
										.setNegativeButton(R.string.cancel_button,
												new DialogInterface.OnClickListener() {
													public void onClick(DialogInterface dialog, int whichButton) {
													}
												});

								dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int whichButton) {

										Repository repo = Repository.open(getActivity());

										Workspace workspace = repo.getWorkspace(((WorkspaceViewModel) workspaceSpinner
												.getSelectedItem()).id);

										try {
											repo.clear(workspace);
										} catch (IllegalStateException ex) {
											Toast.makeText(PointListFragment.this.getActivity(), ex.getMessage(),
													Toast.LENGTH_LONG).show();
											return;
										}

										pointsAdapter.refresh(workspace);

										((WorskpaceAdapter) workspaceSpinner.getAdapter()).refresh();
										workspaceSpinner.setSelection(0);
									}
								});

								dialog.show();
							}
						});
					}

					temp = view.findViewById(R.id.renameWorskpaceButton);
					if (temp != null && temp instanceof Button) {

						((Button) temp).setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								toolDialog.dismiss();
							}
						});
					}

					toolDialog.show();

					return true;
				}
				return false;
			}
		});

		workspaceSpinner.setAdapter(worskpaceAdapter);
		worskpaceAdapter.refresh();
	}

	public void initPointsList() {
		final ListView pointsListView = (ListView) getView().findViewById(R.id.points_list_view);

		pointsAdapter = new PointsAdapter(getActivity(),
				((DroApplication) getActivity().getApplication()).getDro(), R.layout.list_item_point,
				new ArrayList<PointViewModel>());

		// long click set the current point as relative originShape for the
		// workspace
		pointsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1, int position, long id) {
				PointViewModel pointView = (PointViewModel) adapter.getItemAtPosition(position);
				// ActivityHelpers.showPointDetails(getActivity(),
				// pointView.id);

				Point point;
				Repository repo = Repository.open(getActivity());

				try {
					point = repo.getPoint(pointView.id);
				} finally {
					repo.close();
				}
				
				Workspace workspace = ((DroApplication) getActivity().getApplication()).getCurrentWorkspace();

				((DroApplication) getActivity().getApplication()).getDro().setPoint(point);

				workspace.setSelectedPointId(point.getId());
				pointsAdapter.refresh(workspace);
				
				// switch to the DRO screen (when in phone mode)
				((MainActivity) getActivity()).goToReadoutScreen();

			}
		});

		// short click opens point details
		pointsListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View arg1, int position, long id) {

				final PointViewModel pointView = (PointViewModel) adapter.getItemAtPosition(position);

				// outer dialog
				if (pointView != null) {

					View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_point_commands, null);

					// create tools dialog

					Builder toolDialogBuilder = new Builder(getActivity()).setTitle(R.string.point_commands_title)
							.setView(view)
							.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {

								}
							});

					final AlertDialog toolDialog = toolDialogBuilder.create();

					View temp;

					temp = view.findViewById(R.id.addPointButton);
					if (temp != null && temp instanceof Button) {

						((Button) temp).setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								toolDialog.dismiss();

								PointDetailsDialog.ShowPoint(getActivity(), (Integer) null);
							}
						});
					}

					temp = view.findViewById(R.id.editPointButton);
					if (temp != null && temp instanceof Button) {

						((Button) temp).setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								toolDialog.dismiss();

								PointDetailsDialog.ShowPoint(getActivity(), pointView.id);
							}
						});
					}

					temp = view.findViewById(R.id.deletePointButton);
					if (temp != null && temp instanceof Button) {

						((Button) temp).setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								toolDialog.dismiss();

								((DroApplication) getActivity().getApplication()).getCurrentWorkspace().hidePoint(
										pointView.id);
							}
						});
					}

					toolDialog.show();

					return true;
				}
				return false;
			}
		});

		pointsListView.setAdapter(pointsAdapter);

	}

	@Override
	public void onWorkspaceChanged(Workspace sender) {

		worskpaceAdapter.refresh();

		if (sender != null) {
			sender.registerCallback((IPointListChangedCallback) this);
			if (pointsAdapter != null) {
				pointsAdapter.refresh(sender);
			}
		}
	}

	@Override
	public void onPointListChanged(Workspace sender) {
		pointsAdapter.refresh(sender);
	}

	@Override
	public void onModeChanged(Dro sender) {
		pointsAdapter.notifyDataSetChanged();
	}

	@Override
	public void onUnitsChanged(Dro sender) {
		pointsAdapter.notifyDataSetChanged();
	}

}
